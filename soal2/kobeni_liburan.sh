#!/bin/bash

count=1

download(){
    url="https://pixabay.com/get/g4b4994ca9e6f45727bd9935e4e32b28ae60f0a7142e88cebeb2aa49b190ab3b250818a348429c9da1fd37595a8f402a3_640.jpg"
    current_hour=$(date +%H)

    output_folder="kumpulan_$count.FOLDER"

    while [ -d $output_folder ]; do
        ((count++))
        output_folder="kumpulan_$count.FOLDER"
    done

    mkdir "$output_folder"

    if [ $current_hour -eq 0 ]
    then
         current_hour=1
    fi

    for ((i=1;i<=current_hour;i++)); do
        output_file="perjalanan_$i.FILE"
        curl -L $url -o $output_file
        mv $output_file kumpulan_$count.FOLDER/
    done
} 

backup(){
 while [ -d kumpulan_$count.FOLDER ]
  do
    if [ -f devil_$count.zip ]
    then
      ((count++))
      continue
    fi

    zip -r  devil_$count.zip kumpulan_$count.FOLDER
    ((count++))
  done
}


if ["$1" = "download"] 
then
    download
elif ["$1" = "backup"] 
then
    backup
fi

# * */10 * * * /bin/bash /home/gabriella/praktikum/sisop-praktikum-modul-1-2023-am-b13/soal2/kobeni_liburan.sh download
# @daily /bin/bash /home/gabriella/praktikum/sisop-praktikum-modul-1-2023-am-b13/soal2/kobeni_liburan.sh backup
