#!/bin/bash
decrypt=$(date +"%H")
encrypt_file=$(ls -t /home/vboxuser/soal4/encrypt | head -n1)

encrypt_directory="/home/vboxuser/soal4/encrypt/${encrypt_file}"
decrypt_directory="/home/vboxuser/soal4/decrypt/${encrypt_file}"

llowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"

if [ -f "$encrypt_directory" ]; then
 cat "${encrypt_directory}" | tr "${lowercase:${decrypt}:26}${uppercase:${decrypt}:26}" "${lowercase:0:26}${uppercase:0:26}" > "${decrypt_directory}"
fi

# * */2 * * * /bin/bash /home/vboxuser/soal4/log_decrypt.sh

