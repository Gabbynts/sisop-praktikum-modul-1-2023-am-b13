#!/bin/bash
encrypt=$(date +"%H")
encrypted=$(date +"%H:%M %d:%m:%Y")

backup_file="/home/vboxuser/soal4/encrypt/${encrypted}.txt"

lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"

cat /var/log/syslog | tr "${lowercase:0:26}${uppercase:0:26}" "${lowercase:${encrypt}:26}${uppercase:${encrypt}:26}" > "${backup_file}"

# * */2 * * * /bin/bash /home/vboxuser/soal4/log_encrypt.sh
