#!/bin/bash

echo "Masukkan Username :"
read username
if grep -w -q ${username} ./user/users.txt ; then echo "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists">>./log.txt
else
    echo "Buat Password :"

    while [ true ]; do
	    
        read password
	    if [[ ${#password} -lt 8 ]]; then echo "Password setidaknya memiliki 8 karakter, masukkan password baru"
		    continue
	    fi
        
	    if [[ ! "$password" =~ [[:upper:]] ]]; then echo "Password setidaknya memiliki 1 huruf uppercase, masukkan password baru"
		    continue
	    fi
        
	    if [[ ! "$password" =~ [[:lower:]] ]]; then echo "Password setidaknya memilliki 1 huruf lowercase, masukkan password baru"
		    continue
	    fi
        
	    if [[ ! "$password" =~ [[:digit:]] ]]; then echo "Password setidaknya memiliki 1 angka, masukkan password baru"
		    continue
	    fi
	
		if echo ${password} | grep -Eq "chicken|ernie"; then echo "Password tidak boleh menggunakan kata chicken atau ernie"
			continue
		fi 

        if [[ ${password} == ${username} ]] ; then echo "Password tidak boleh sama dengan username, masukkan password baru"
		    continue
	    fi
         
	    break
    done

    echo "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully">>log.txt
    echo "$username:$password">>users.txt

fi
