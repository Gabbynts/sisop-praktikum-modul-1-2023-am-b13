#!/bin/bash
echo "Poin 1 : Tampilkan 5 Universitas dengan ranking tertinggi di Jepang"
awk -F, '{if($4=="Japan")print $1"|"$2}' '2023 QS World University Rankings.csv' | head -n5
echo "====================================================="
echo "Poin 2 : Tampilkan 5 Universitas dengan Faculty Student Score (fsr score) terendah di Jepang"
awk -F, '{if($4=="Japan")print $2"|"$9}' '2023 QS World University Rankings.csv' | sort -t '|' -k2n,2 | head -n5
echo "====================================================="
echo "Poin 3 : Tampilkan 10 Universitas dengan Employment Outcome Rank (ger rank) tertinggi di Jepang"
awk -F, '{if($4=="Japan")print $2"|"$20}' '2023 QS World University Rankings.csv' | sort -t '|' -k2n,2 | head -n10
echo "====================================================="
echo "Poin 4 : Tampilkan Universitas dengan kata kunci Keren"
awk -F, '/Keren/{print $2}' '2023 QS World University Rankings.csv'
