# sisop-praktikum-modul-1-2023-AM-B13

## Praktikum modul 1 

### Kelompok B-13 dengan anggota:
| **No** | **Nama** | **NRP** | 
| ------------- | ------------- | --------- |
| 1 | Gabriella Natasya Br Ginting  | 5025211081 | 
| 2 | Muhammad Zikri Ramadhan | 5025211085 |
| 3 | Sandhika Surya Ardyanto | 5025211022 |

### Daftar Isi
- [Soal 1](#soal-1)
    - [a. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang](#perintah-1-a)
    - [b. Cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang](#perintah-1-b)
    - [c. Cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi](#perintah-1-c)
    - [d. membantu bocchi mencari universitas tersebut dengan kata kunci keren](#perintah-1-d)
- [Soal 2](#soal-2)
    - [a. Download gambar setiap 10 jam sekali mulai dari saat script dijalankan](#perintah-2-a)
    - [b. Melakukan zip setiap 1 hari](#perintah-2-b)
- [Soal 3](#soal-3)
    - [a. Memastikan password pada register dan login aman](#perintah-3-a)
    - [b. Percobaan login dan register akan tercatat pada log.txt](#perintah-3-b)
- [Soal 4](#soal-4)
    - [a. Backup file log system](#perintah-4-a)
    - [b. Isi file yang dienkripsi dengan string manipulation](#perintah-4-b)
    - [c. Script untuk dekripsi](#perintah-4-c)
    - [d. Backup file syslog setiap 2 jam](#perintah-4-d)
    
## Soal 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut.

Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi.

 - awk merupakan command padda Linux yang berfungsi untuk memanipulasi teks dengan filtering teks berdasarkan pola tertentu
 - -F merupakan bagian dari command awk yang berfungsi sebagai pemisah antar kolom pada file. Pada soal 1 karena berupa file csv maka pemisahnya adalah "," sehingga menjadi -F,

- ### Perintah 1-a
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.
```
awk -F, '{if($4=="Japan")print $1"|"$2}' '2023 QS World University Rankings.csv' | head -n5
```

- if($4=="Japan") berfungsi sebagai filter pada command awk dimana kolom 4 "location" merupakan "Japan"
- print $1"|"$2 berfungsi mencetak kolom 1 yang merupakan urutan ranking universitas dan kolom 2 merupakan nama universitas pada file csv. Karena sudah terurut berdasarkan kolom 1 maka tidak perlu command sort
- 2023 QS World University Rankings.csv merupakan nama file yang difilter oleh command awk
- Operator | yaitu pipe untuk output dari awk menjadi input untuk head -n5 
- head -n5 merupakan command Linux yang berfungsi untuk mencetak hanya 5 baris pertama dari output awk 

- ### Perintah 1-b
Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 
```
awk -F, '{if($4=="Japan")print $2"|"$9}' '2023 QS World University Rankings.csv' | sort -t '|' -k2n,2 | head -n5 
```
- if($4=="Japan") berfungsi sebagai filter pada command awk dimana kolom 4 "location" merupakan "Japan"
- print $1"|"$9 berfungsi mencetak kolom 2 merupakan nama universitas dan kolom 9 yang merupakan urutan fsr score terendah hingga tertinggi pada file csv. 
- 2023 QS World University Rankings.csv merupakan nama file yang difilter oleh command awk
- Operator | yaitu pipe untuk output dari awk menjadi input untuk sort
- sort -t '|' -k2n,2 berfungsi untuk mengurutkan berdasarkan kolom 2 dari output awk. Bagian n pada -k2n,2 berfungsi untuk mengurutkan berdasarkan data numerik
- head -n5 merupakan command Linux yang berfungsi untuk mencetak hanya 5 baris pertama dari output sort

- ### Perintah 1-c
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
```
awk -F, '{if($4=="Japan")print $2"|"$20}' '2023 QS World University Rankings.csv' | sort -t '|' -k2n,2 | head -n10
```
- if($4=="Japan") berfungsi sebagai filter pada command awk dimana kolom 4 "location" merupakan "Japan"
- print $1"|"$20 berfungsi mencetak kolom 2 merupakan nama universitas dan kolom 20 yang merupakan urutan ger rank pada file csv 
- 2023 QS World University Rankings.csv merupakan nama file yang difilter oleh command awk
- Operator | yaitu pipe untuk output dari awk menjadi input untuk head -sort 
- sort -t '|' -k2n,2 berfungsi untuk mengurutkan berdasarkan kolom 2 dari output awk. Bagian n pada -k2n,2 berfungsi untuk mengurutkan berdasarkan data numerik
- head -n10 merupakan command Linux yang berfungsi untuk mencetak hanya 10 baris pertama dari output sort

- ### Perintah 1-d
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.
```
awk -F, '/Keren/{print $2}' '2023 QS World University Rankings.csv
```
- /Keren/ berfungsi sebagai filter pada command awk dimana pada 1 baris per kolom terdapat kata "Keren"
- print $2 berfungsi untuk mencetak kolom 2 yang merupakan nama universitas pada file csv 
- 2023 QS World University Rankings.csv merupakan nama file yang difilter oleh command awk

## Soal 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 

- ### Perintah 2-a
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. 

Untuk mendapatkan X (jam sekarang), menggunakan:
```
current_hour=$(date +%H)
```
- date untuk mengambil data waktu
- %H untuk menampilkan jam

Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

1. File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)

Melakukan for loop untuk memberi keterangan nomor pada penamaan file gambar yang di download, dengan looping yang dilakukan sebanyak nilai current_hour / jam pada saat script di jalankan.

```
for ((i=1;i<=current_hour;i++)); do
    output_file="perjalanan_$i.FILE"
    curl -L $url -o $output_file
done
```

Apabila script dijalankan pada saat jam 00.00 maka gambar akan di download satu saja. Sehingga dapat dilakukan if condition yang dimana apabila current_hour bernilai 0, maka current_hour akan di set menjadi 1.

```
if [ $current_hour -eq 0 ]
then
    current_hour=1
fi
```

2. File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.-FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 

Mefinisikan variabel count untuk mencatat banyaknya folder yang dibuat
```
count=1
```

Folder dibuat dengan:

```
output_folder="kumpulan_$count.FOLDER"
mkdir "$output_folder"
```
- mkdir: command untuk membuat directory

Untuk melakukan penamaan folder, dibutuhkan while loop yang dimana berfungsi untuk mengecek apakah folder yang dibuat belum memiliki nama yang sama dengan folder yang sudah dibuat sebelumnya.

Dilakukan while loop untuk menghindari adanya penamaan folder yang sama.

```
while [ -d $output_folder ]; do
    ((count++))
    output_folder="kumpulan_$count.FOLDER"
done
```

File yang dibuat akan dimasukkan ke folder yang sudah dibuat sebelumnya.
```
mv $output_file kumpulan_$count.FOLDER/
```

- ### Perintah 2-b
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. 

Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). 

Untuk melakukan zip, dapat dilakukan dengan:
```
zip -r devil_$count.zip kumpulan_$count.FOLDER
```

Code diatas akan membuat zip dengan isi folder gambar.

Lalu lakukan while loop, karena penamaan zip dapat dilakukan dengan “devil_NOMOR ZIP”, yang dimana penomoran dilakukan sesuai dengan penomoran folder. 

```
while [ -d kumpulan_$count.FOLDER ]
  do
    zip -r  devil_$count.zip kumpulan_$count.FOLDER
    ((count++))
  done
```

Sama dengan kondisi penamaan folder, untuk menghindari adanya nama zip yang sama. Maka, dilakukan if condition. Sehingga, apabila nomor pada penamaan sudah ada, maka count akan di increment

```
if [ -f devil_$count.zip ]
then
    ((count++))
    continue
fi
```

Pada soal diminta untuk mendownload gambar setiap 10 jam sekali dan zip setiap hari, maka dapat memakai cronjob:

```
* */10 * * * /bin/bash /home/gabriella/praktikum/sisop-praktikum-modul-1-2023-am-b13/soal2/kobeni_liburan.sh download

@daily /bin/bash /home/gabriella/praktikum/sisop-praktikum-modul-1-2023-am-b13/soal2/kobeni_liburan.sh backup
```

Karena diminta untuk menjalankan 2 cronjob pada satu file, maka untuk download gambar dan zip dibedakan menjadi 2 fungsi, yaitu fungsi download dan backup.

```
download(){
    ...
} 

backup(){
    ...
}

if ["$1" = "download"] 
then
    download
elif ["$1" = "backup"] 
then
    backup
fi
```

## Soal 3
Membuat 2 file register, log.txt dan users.txt. Log berisi catatan informasi dari percobaan register dan login. Users berisi data username dan password tiap registrasi yang berhasil dilakukan.

Pada louis.sh, dilakukan pengecekan apakah username yang ingin digunakan sudah ada atau belum pada file users.txt

```
if grep -q "^${username}" /users/users.txt ; then echo "$(date +"%y/%m/%d %T") REGISTER: ERROR User already exists">>./log.txt
```

Jika username yang akan digunakan sudah ada di dalam file users.txt, artinya username tersebut sudah ada yang menggunakan dan tidak dapat digunakan lagi. Pesan yang tercatat adalah "REGISTER: ERROR User already exists"

### Perintah 3-a
Jika username belum digunakan, maka akan lanjut ke pembuatan password.
Password akan dicek apakah sesuai dengan ketentuan atau tidak.

1. Password minimal terdiri atas 8 karakter

```
if [[ ${#password} -lt 8 ]]; then echo "Password setidaknya memiliki 8 karakter, masukkan password baru"
```

Jika password memiliki karakter kurang dari 8, maka akan diminta membuat password baru

2. Password setidaknya memiliki 1 karakter uppercase

```
if [[ ! "$password" =~ [[:upper:]] ]]; then echo "Password setidaknya memiliki 1 huruf uppercase, masukkan password baru"
```

":upper:" digunakan untuk mencek karakter uppercase, jika tidak ada, akan diminta membuat password baru

3. Password setidaknya memiliki 1 karakter lowercase

```
if [[ ! "$password" =~ [[:lower:]] ]]; then echo "Password setidaknya memilliki 1 huruf lowercase, masukkan password baru"
```

":lower:" digunakan untuk mencek karakter lowercase, jika tidak ada, akan diminta membuat password baru

4. Password tidak boleh mengandung kata chicken dan ernie

```
if echo ${password} | grep -Eq "chicken|ernie"; then echo "Password tidak boleh menggunakan kata chicken atau ernie"
			continue
	fi 
```

5. Password tidak boleh sama dengan username

```
if [[ "$password" == "$username" ]]; then echo "Password tidak boleh sama dengan username, masukkan password baru"
```

Jika password sama dengan username, maka akan diminta membuat password baru

break dilakukan hanya ketika password tidak memenuhi kondisi "if" yang mana pun, ini berarti bahwa password sudah sesuai dengan ketentuan, maka kita bisa keluar dari looping.

Setelah keluar dari looping, username dan password baru sudah berhasil dibuat, maka akan dicatat aktivitas registernya ke file log.txt dan informasi username dan passwordnya ke dalam file users.txt

```
echo "$(date +"%y/%m/%d %T") REGISTER: INFO User $username registered successfully" >> ./log.txt
echo "$user_name:$user_password" >> ./users/users.txt
```
### Perintah 3-b

Tiap percobaan login akan tercatat pada log.txt
Pada bagian 3-a, sudah dicantumkan case apabila ketika register namun username sudah ada, serta log jika berhasil membuat akun. Pada retep.sh akan dicover 2 case lainnya, yaitu log ketika login dengan salah password, serta login yang berhasil

1. Login berhasil dengan username dan password yang benar

```
if grep -w -q "${username}:${password}" ./user/users.txt ; then echo "$tanggal LOGIN:INFO User $username logged in" >>log.txt
```

Akan dibandingkan antara username dan password yang diberikan dengan username dan password ppada file users.txt. Ketika username dan password cocok, maka login berhasil

2.  Login gagal karena salah memasukkan password

```
else
echo "$tanggal LOGIN:ERROR Failed login attempt on user $username" >>log.txt
```

Jika tidak ada username dan password yang benar, maka login akan gagal
## Soal 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. 

File syslog tersebut harus memiliki ketentuan : 

- ### Perintah 4-a
Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).

```
encrypted=$(date +"%H:%M %d:%m:%Y")
```
- date untuk mengambil data waktu
- %H untuk menampilkan jam
- %M untuk menampilkan menit
- %d untuk menampilkan tanggal
- %m untuk menampilkan bulan
- %Y untuk menampilkan tahun

- ### Perintah 4-b
Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
 1. Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
 2. Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
 3. Setelah huruf z akan kembali ke huruf a

File yang di backup akan di masukkan ke directory encrypt dan disimpan dalam bentuk .txt sesuai dengan path dibawah.
```
backup_file="/home/vboxuser/soal4/encrypt/${encrypted}.txt"
```

Definisikan abjad dengan abjad kapital serta kecil.
```
lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
```

Menggunakan command cat untuk membaca file syslog
```
cat /var/log/syslog 
```

Mengubah abjad sesuai dengan penjumlahan jam pada script dijalankan. Serta memasukkan file sesuai dengan backup path
```
cat /var/log/syslog | tr "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" > "${backup_filename}"
```
- "|" digunakan untuk menghubungkan ouput dari command cat ke tr
- command tr untuk mentransformasi karakter pada abjad

- ### Perintah 4-c
Buat juga script untuk dekripsinya.

Variabel decrypt sebagai penyimpan nilai dari jam dijalankannya file log_decrypt.sh untuk mendeskripsikan file log_encrypt.sh karena dijalankan di waktu yang bersamaan 

```
decrypt=$(date +"%H")
```
- Command ls -t berfungsi untuk menampilkan direktori atau file yang terdapat pada path 
- head -n1 untuk membatasi 1 file yang dipilih commanf ls -t. Hal ini karena file log_encrypt.sh dan lod_decrypt.sh dijalankan bersamaan melalui crontab setiap 2 jam sekali
- Variabel encryt_file untuk menyimpan file pada path /home/vboxuser/soal4/encrypt

```
encrypt_file=$(ls -t /home/vboxuser/soal4/encrypt | head -n1)
```

Variabel encrypt_directory untuk menyimpan path tempat file hasil enkripsi di direktori:

```
encrypt_directory="/home/vboxuser/soal4/encrypt/${encrypt_file}" 
```
Variabel decrypt_directory untuk menyimpan path tempat file hasil deskripsi di direktori:

```
decrypt_directory="/home/vboxuser/soal4/decrypt/${encrypt_file}"
```

Pada bagian huruf biasa dan kapital diketik 2 kali dari a-z untuk dijadikan dasar enkripsi dan deskripsi. Oleh karena jumlah jam maksimal hanya 23 dan jumlah alphabet ada 26. Jika jam 23 maka a menjadi x dan z menjadi w dengan w pada barisan merupakan urutan ke 49. Dimana jumlah alphabet 2 kali dapat menampung karena lebih besar daripada jumlah jam

```
lowercase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
uppercase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
```

- if [ -f "$encrypt_directory" ] digunakan untuk mengecek apa file di encrypt_directory atau file yang akan dideskripsi ada
- cat "${encrypt_directory}" berfungsi untuk menampilkan isi file dari encrypt_directory
- Operator | untuk menjadikan output dari cat menjadi input untuk tr
- Command tr merupakan command di Linux untuk mentranslate teks. Misal jam diketahui 15 maka barisan "pqrstuvwxyzabcdefghijklmno" menjadi "abcdefghijklmnopqrstuvwxyz"
- Hasil deskripsi berdasarkan posisi decrypt sebanyak 26 yaitu jumlah alphabet menjadi urutan alphabet semula. Hasil deskripsi di redirect dengan > ke decrypt_directory

``` 
if [ -f "$encrypt_directory" ]; then
 cat "${encrypt_directory}" | tr "${lowercase:${decrypt}:26}${uppercase:${decrypt}:26}" "${lowercase:0:26}${uppercase:0:26}" > "${decrypt_directory}"
fi
```

- ### Perintah 4-d
Backup file syslog setiap 2 jam untuk dikumpulkan

Crontab untuk mengeksekusi file log_decrypt.sh dan log_encrypt.sh setiap 2 jam sekali

```
* */2 * * * /bin/bash /home/vboxuser/soal4/log_encrypt.sh
* */2 * * * /bin/bash /home/vboxuser/soal4/log_decrypt.sh 
```